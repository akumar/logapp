# 
# Logapp example configuration file
# 
# The search path for the configuration file is the following:
#   ~/.logapprc
#   /etc/logapp.conf
#   /etc/logapp/logapp.conf
#
# The fixed locations can also be overridden with the --configfile command.
# For example: logapp --configfile=example.conf ...

# Generic configuration for all sections

logfile       = "~/logapp.log" # default logfile if no application specific
                               # configuration exists
appendlog     = false          # logfile will be truncated on every session
logrename     = true           # rename old logfile before truncating it
warnlogfilelock = true         # warn if logfile is renamed because of a lock
oldlogext     = ".old"         # old logfiles will get the extension .old
circularlog   = false          # no circular logfile
maxlogsize    = 0              # no logfile size limit (value is in kiB)
locklogfile   = true           # lock the logfile after opening it to prevent
                               # two logapp sessions writing to it at the same
                               # time
maxaltlogfiles= 9              # allow to append the numbers 0-9 to the logfile
                               # name in the case that the previous files exist
                               # and are not accessible due to locking
stdout_lineprefix = ""         # no lineprefix for stdout lines
stderr_lineprefix = "STDERR: " # prefix stderr lines with STDERR: in the log
alignlinebreaks   = true       # format charbased data for better reading

extended-regexp     = false    # interpret basic regular expressions like grep

stdout_fgcol        = default  # use console default color for stdout
stdout_bold         = false    # normal font for stdout
stdout_regexp_bgcol = blue     # blue background color on regexp match
stderr_fgcol        = red      # use red for errors and warnings
stderr_bold         = true     # bold font for stdout
stderr_regexp_bgcol = blue     # blue background color on regexp match

print_summary       = true     # print short summary on exit
print_logname       = false    # don't always print the name of the used
                               # logfile after execution


# Application specific configuration
#
# You can either provide a full path or only the application name to
# start a new section.
#
# Usually the section will be choosen by the executable name, but you can
# override it with the --configsection option.

[make]
logfile           = "~/make.log"
disable_keywords  = "menuconfig" # "make menuconfig" won't be processed by
                                 # logapp
stderr_regexp     = "error:"     # highlight lines that contain the string
                                 # "error:"

[svn]
logfile           = "~/svn.log"
stdout_regexp     = "^[MC] *"    # highlight merges and conflicts
detectescape      = true         # switch to charbased mode if an editor is
                                 # started

[cvs]
logfile           = "~/cvs.log"
stdout_regexp     = "^[MC] *"    # highlight merges and conflicts
detectescape      = true         # switch to charbased mode if an editor is
                                 # started

[telnet]
logfile           = "~/telnet.log"
stdout_charbased  = true         # charbased mode is enabled for stdout
stderr_charbased  = false        # charbased mode is disabled for stderr
alignlog          = true         # use one line per written data package
logtime           = true         # use timestamps in the logfile
logreltime        = false        # log absolute times
jointimeout       = 0            # disable packet joining

[picocom]                        # picocom is a minimal terminal emulator...
logfile           = "~/picocom.log"
stdout_charbased  = true         # charbased mode is enabled for stdout
stderr_charbased  = false        # charbased mode is disabled for stderr
alignlog          = true         # use one line per written data package
logtime           = true         # use timestamps in the logfile
logreltime        = true         # log relative times instead of absolute ones
jointimeout       = 10           # join packets to lines if they are received
                                 # within 10 ms
