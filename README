-------------------------------------------------------------------------------
Logapp README
-------------------------------------------------------------------------------

Logapp is a wrapper utility that helps supervise the execution of applications
that produce heavy console output (e.g. make, CVS, and Subversion). It does
this by logging, trimming, and coloring each line of the output before
displaying it. It can be called instead of the executable that should be
monitored; it then starts the application and logs all of its console output
to a file. The output shown in the terminal is preprocessed, e.g. to limit the
length of printed lines and to show the stderr output in a different color.
It is also possible to automatically highlight lines that match a certain
regular expression. The output is therefore reduced to the necessary amount,
and all important lines are easy to identify.

For new versions go to http://logapp.sourceforge.net/

You can always get the latest development version via Subversion:
  svn co https://logapp.svn.sourceforge.net/svnroot/logapp/trunk logapp


Installation
------------

Simply extract the provided source package and call "make" inside the created
directory to compile the logapp executable. "make install" will install the
executable into "/usr/local/bin" and a manpage to "/usr/local/share/man/man1".
Additionally it will create symbolic links called logmake, logcvs and logsvn
that point to logapp.

You will need libpthread and libutil to compile this version of the application
as it uses posix threads and a pseudo terminal for console redirection. If you
want to use logapp without this dependencies you can edit the Makefile and
disable the options SUPPORT_PTY and/or USE_THREADS.

The destination path for the installation can be changed by defining the PREFIX
environment variable or by changing the Makefile directly.

Calling "make help" will print a short overview of the available Makefile
targets and environment variables.

Usage
-----

logapp can be used in two ways:

  1. Call the logapp executable with the application as parameter:
  
        $ logapp echo "Hello World!"
        Hello World!

        All arguments after the executable name are ignored by logapp and
        provided to the application unless they start with "--logapp_".

  2. Create a symlink with the applications name to the logapp executable:
        
        $ ln -s /usr/local/bin/logapp echo
        $ ./echo "Hello World!"
        Hello World!

        You can also add a log* prefix to the symlink to get the same effect:

        $ ln -s /usr/local/bin/logapp logecho
        $ ./logecho "Hello World!"
        Hello World!

        Symbolic links from logapp to logmake, logcvs and logsvn will be
        created automatically by the installation routine.

Additionally you can create an alias for calling logapp automatically when
you normally would execute the original application:
  
  $ alias make="logapp make"
  $ make

  $ alias make="logmake"
  $ make

Parameter passing:

  If calling logapp directly, it will parse all arguments that are placed
  before the application filename. Arguments after that are directly passed to
  the application.
  Arguments added when calling a symlink are directly passed to the application.

  There is one exception: When prefixing arguments with "--logapp_" they will
  always be parsed by logapp. You can add --logapp_ to all long argument names
  provided by logapp.

      To get the logapp help instead of running an application you have the
      following possibilities:

      $ logapp -?
      $ logapp --help
      $ logapp --help make
      $ logapp make --logapp_help
      $ logmake --logapp_help
  
  See "logapp --help" or "man logapp" for all currently supported parameters.

Function overview:

  Please have a look into the logapp manpage (man 1 logapp) for a detailed
  description of all supported options.

After calling logapp, it will create a file called logapp.log in the current
directory where all application output is stored together with some additional
information. The file will be recreated every time you restart logapp if the
--appendlog option is not set to true. You can modify the filename/path with
the --logfile parameter.

The logfile will start with a header that shows which application is run with
what commandline options, the working path and the current time. With the
--logenv option set to true, additionally a list of all active environment
variables will be added to the header. After the program exited, a footer will
be added to the file that shows the exit status, the line count and the time.
Additionally each line of the logfile can be prefixed with a custom string by
using the --std<out|err>_lineprefix parameter. The default is that stdout
output isn't prefixed and stderr output starts with "STDERR: ".

You can limit the logfile size with the --maxlogsize parameter. There are
two ways how this can be achieved: By truncating the logfile if the size
is reached or by rewriting the logfile from the beginning (overwriting the old
content). The first way is best used with the --logrename option set to true,
this will make a copy of the old logfile with the extension ".log" added. In
way the maximum size logfiles will take is two times the --maxlogsize value.
The second way is selected by setting the --circularlog parameter to true.
There will be additional tags included in the logfile to make it possible
navigating through the data.

With the --logtime option set to true there will be ms timestamps added to
each line of the logfile. Those can also show the relative time since the last
line if --logreltime is true.
With --logtime_rstregexpr it is also possible to reset the logged time on a
match of the regular expression. This will also work together with the
--logreltime option and set the logged relative time to zero. 

The default for console output is that all stdout lines are limited to the
console width. stderr output is not limited and is printed red and bold. You
can change the default by providing the --std<out|err>_fgcol and
--std<out|err>_bold parameters.
      
    Valid color values are: -1  (console) default
                             0  black
                             1  red
                             2  green
                             3  brown
                             4  blue
                             5  magenta
                             6 	cyan
                             7  white

The text coloring is automatically disabled if a "dumb" console is detected,
for example if you are calling logapp from inside an other application like vi.

With the --std<out|err>_regexp parameter you can provide a regular expression
where matching lines are highlighted on console output.

It is also possible to get a short summary after the application exited by
setting the print_summary option to true.

  $ logapp --stderr_fgcol=brown --stderr_bold=true --print_summary=true \
           --logfile=/tmp/make.log make

  This line will call make and create brown error messages in a bold font,
  print a summary after exit and save all output into /tmp/make.log. If you set
  this line as alias you can simply call make to get the same result.

The logapp console output processing can be disabled with the --disable option.
This can be useful for example if you start curses based applications like the
menuconfig tool of the Linux kernel directly with make. As those applications
are not line based the screen would otherwise get messed up.
You can also provide a comma separated list of disable keywords with
--disable_keywords. The arguments of the application will be checked if they
contain one of those keywords.

If you want to use logapp with applications that can't be used with line-based
data handling you can try to use char-based stream handling. With this option
enabled line clipping and coloring won't work, but the data gets still logged.
This option can be enabled for both streams independently by setting the
--<stdout|stderr>_charbased parameters to true. If you want to use this option
together with the timestamp function you should have a look at the --alignlog
and --jointimeout options.
You can also enable the --detectescape option so logapp will automatically
switch to charbased mode for a specific stream as soon as it detects an escape
sequence in the data.

Configuration file:

  You can put the long parameters and their values into a configuration file.

  The search path is: ~/.logapprc, /etc/logapp.conf, /etc/logapp/logapp.conf
  
  It is also possible to use a custom config file path defined with the
  --configfile option.

  Have a look at the sample configuration file called example.conf provided
  with the source for more information.

Example
-------

Put the following lines in your ~/.bashrc file (provided you are using bash):

    alias make='logapp -F red -B true -c 80 -l=~/make.log make'
    alias svn='logapp -F red -B true -c 80 -l=~/svn.log svn'
    alias cvs='logapp -F red -B true -c 80 -l=~/cvs.log cvs'

This will let bash automatically use logapp to call make, svn and cvs from the
console and write the output into a logfile in your home directory. The normal
output of the applications will be clipped at 80 chars and errors/warnings will
be shown in red with bold font. You can get the same result by putting the
configuration options into application specific sections inside the config
file.
