Source: logapp
Section: utils
Priority: optional
Maintainer: Kumar Appaiah <akumar@debian.org>
Build-Depends: quilt
Rules-Requires-Root: no
Standards-Version: 4.4.1
Homepage: http://logapp.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/akumar/logapp
Vcs-Git: https://salsa.debian.org/akumar/logapp.git

Package: logapp
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: supervise execution of applications producing heavy output
 Logapp is a wrapper utility that helps supervise the execution of
 applications that produce heavy console output (e.g. make, CVS and
 Subversion). It does this by logging, trimming, and coloring each
 line of the output before displaying it. It can be called instead of
 the executable that should be monitored; it then starts the
 application and logs all of its console output to a file. The output
 shown in the terminal is preprocessed, e.g. to limit the length of
 printed lines and to show the stderr output in a different color. It
 is also possible to automatically highlight lines that match a
 certain regular expression. The output is therefore reduced to the
 necessary amount, and all important lines are easy to identify.
