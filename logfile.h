/*
 * logfile.h:  logapp logfile handling
 *
 *
 * Copyright (C) 2007-2009 Michael Brunner <mibru@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * General Public License for more details.
 */

#ifndef __LOGFILE_H__
#define __LOGFILE_H__

#include "logapp.h"

#include <stdlib.h>

#include "configuration.h"

extern int logfile_write(logfile_t* logfile, pipe_t* pipe);
extern int logfile_open(char* filename, logfile_t* logfile);
extern int logfile_close(logfile_t* logfile);

#endif /* __LOGFILE_H__ */
