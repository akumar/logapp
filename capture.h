/*
 * capture.c: capture data from stdout and stderr
 *
 *
 * Copyright (C) 2007 Michael Brunner <mibru@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * General Public License for more details.
 */

#ifndef __CAPTURE_H__
#define __CAPTURE_H__

void reset_console(void);
int capture_start(void);
int capture_end(void);
void print_summary(void);

#endif /* __CAPTURE_H__ */
