/*
 * logapp.h: Logapp main header file
 *
 *
 * Copyright (C) 2007-2010 Michael Brunner <mibru@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * General Public License for more details.
 */

#ifndef __LOGAPP_H__
#define __LOGAPP_H__

#define AUTHOR		"Michael Brunner <mibru@gmx.de>"

typedef enum { MESSAGE, WARNING, ERROR } t_printtype;
#define message(x...)	print(MESSAGE, x)
#define warning(x...)	print(WARNING, x)
#define error(x...)	print(ERROR, x)
#define STRING_WARNING	" warning: "
#define STRING_ERROR	" error: "
void print(t_printtype type, const char *format,...);
void error_outofmemory(void);
int execcmd(char* cmd);

/* These values should normally be defined by the Makefile */
#ifndef EXECUTABLE
# define EXECUTABLE	"logapp"
#endif
#ifndef VERSION
# define VERSION	""
#endif
#ifndef SVN_REVISION
# define SVN_REVISION	""
#endif
#ifndef DEBUG_BUILD
# define DEBUG_BUILD 0
#endif
#ifndef CONFIG_SUPPORT_PTY
# define CONFIG_SUPPORT_PTY 1
#endif
#ifndef CONFIG_USE_THREADS
# define CONFIG_USE_THREADS 1
#endif


#if (CONFIG_USE_THREADS == 1)
/* Let the system take care of making errno thread safe. */
#define _REENTRANT
#endif

#endif /* __LOGAPP_H__ */
