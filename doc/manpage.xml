<?xml version="1.0" encoding="utf8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

<refentry id='logapp'>
  <refentryinfo>
    <date>January 2011</date>
  </refentryinfo>
  
  <refmeta>
    <refentrytitle><application>logapp</application></refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo>logapp 0.16svn</refmiscinfo>
  </refmeta>

  <refnamediv id='head'>
    <refname><application>logapp</application></refname>
    <refpurpose>An application output supervisor.</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <para>
      <cmdsynopsis>
        <command>logapp</command>
        <arg choice='opt' rep='repeat'><replaceable>option</replaceable></arg>
        <arg choice='plain'><replaceable>application</replaceable></arg>
        <arg choice='opt' rep='repeat'><option>&minus;&minus;logapp_<replaceable>option</replaceable></option></arg>
        <arg choice='opt' rep='repeat'><replaceable>app.-argument</replaceable></arg>
      </cmdsynopsis>
    </para>

    <para>
      <cmdsynopsis>
        <command>applicationsymlink</command>
        <arg choice='opt' rep='repeat'><option>&minus;&minus;logapp_<replaceable>option</replaceable></option></arg>
        <arg choice='opt' rep='repeat'><replaceable>application-argument</replaceable></arg>
      </cmdsynopsis>
    </para>

    <para>Instead of calling logapp directly you can also create a symlink with
      the name of the application pointing to logapp. Logapp will automatically
      start the application the name points to. It will also work if the
      symlink name is prefixed with <emphasis>log</emphasis>.
    </para>
  </refsynopsisdiv>

  <refsect1 id="description">
    <title>DESCRIPTION</title>
    <para>Logapp is a wrapper utility that helps supervise the execution of applications that produce heavy console output (e.g. make, CVS and Subversion). It does this by logging, trimming, and coloring each line of the output before displaying it. It can be called instead of the executable that should be monitored; it then starts the application and logs all of its console output to a file. The output shown in the terminal is preprocessed, e.g. to limit the length of printed lines and to show the stderr output in a different color. It is also possible to automatically highlight lines that match a certain regular expression.  The output is therefore reduced to the necessary amount, and all important lines are easy to identify.</para>
  </refsect1>

  <refsect1 id="options">
    <title>OPTIONS</title>

    <para>The options provided before the <replaceable>application</replaceable> argument are processed directly by logapp. Options provided after the <replaceable>application</replaceable> argument are only parsed if they are prefixed with <option>--logapp_</option> (long option names only) otherwise they are passed to the application. If logapp is called via a symlink all unprefixed options are passed to the application.</para>
    <para>Every application usually uses two independent output streams: <emphasis>stdout</emphasis> for normal output and <emphasis>stderr</emphasis> for errors and important messages. Both of them are handled independently by logapp, therefore many options are available for both streams.</para>
    <para>Bool options are accepting <emphasis>1/0</emphasis> and <emphasis>true/false</emphasis> as value. For long boolean options the value can be omitted, in that case it will be assumed to be &apos;true&apos;.</para>
    <refsect2 id="general_options">
      <title>GENERAL OPTIONS</title>
      <variablelist>

        <varlistentry>
          <term><option>-?</option></term>
          <term><option>--help</option></term>
          <listitem>
            <para>Show a short overview over all available options.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--version</option></term>
          <listitem>
            <para>Show version information.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--configfile=</option><replaceable>FILE</replaceable></term>
          <listitem>
            <para>Use a specific configuration file instead of searching the configuration search paths.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--showconfig</option></term>
          <listitem>
            <para>Print the current configuration of logapp and exit before the application is executed. This can be used this to check if all configuration options are setup correctly if something doesn't work as expected.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--configsection=</option><replaceable>NAME</replaceable></term>
          <listitem>
            <para>Enable a specific section in the configuration file. If this option is not provided the application name is used as default.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--disable</option></term>
          <listitem>
            <para>This disables logapp data handling completely. The application is still started, but logapp won't touch the data streams coming from the application. Neither logging nor output formating is performed, only the execution time and the exit state tracked. This is useful if logapp won't be able to deal with expected data correctly, for example when starting curses based applications. Have a look at <option>--disable_keywords</option> to see how this option can be enabled automatically.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--disable_keywords=</option><replaceable>keywordlist</replaceable></term>
          <listitem>
            <para>With this option a list of comma separated keywords can be provided which will cause the <option>--disable</option> to be enabled automatically if found in the applications option list. This is useful if an application usually provides line-based output, but creates binary data or uses a curses based frontend if called with a specific parameter. You can also use the <option>--detectescape</option> option for another way to do this without disabling the logging functionality.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--detectescape=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option can be used to switch escape-sequence detection on or off. With escape-sequence detection logapp will automatically enable char-based stream handling as soon as an escape-sequence is part of the specific stream. This behavior can be useful if you are working with an application that is usually line-based, but starts other applications which may be using escape sequences to format the screen. This option will prevent the terminal from being messed up in that case.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--dumbterm=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>With this option set to true there will be no terminal output coloring for <emphasis>stdout</emphasis> and <emphasis>stderr</emphasis>. Normally this option is disabled and logapp tries to detect "dumb" terminals itself.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--usepty=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option is only available if logapp has been compiled with PTY support. If PTY support is enabled with this option set to true, logapp will open a <emphasis>pseudo terminal</emphasis> for <emphasis>stdout</emphasis>. This helps wenn running logapp with applications that usually need a real terminal for output. You can disable this option for most line based applications like make, CVS or Subversion. Other applications like telnet or picocom may produce strange results when used without PTY support.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--ptyremovecr=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option is only available if logapp has been compiled with PTY support. When using a pseudo terminal for getting the application output you will always get CR-LF line endings, which is usually not desired when working in UNIX environments. With this option enabled, logapp will automatically translate all CR-LF line endings in LF line endings. This option is enabled as default.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--stdout_blen=</option><replaceable>bytes</replaceable></term>
          <listitem><anchor id="stdout_blen"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--stderr_blen=</option><replaceable>bytes</replaceable></term>
          <listitem>
            <para>The line buffer size can be adjusted for <emphasis>stdout</emphasis> and <emphasis>stderr</emphasis> independently with this option. If the value is too small, lines will be split up if the buffer is full. The default is <emphasis>2048 byte</emphasis> which should be big enough for most applications.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--stdout_charbased=</option><replaceable>bool</replaceable></term>
          <listitem><anchor id="stdout_charbased"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--stderr_charbased=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>If you want to use logapp with applications that do not produce line based output you can enable this options for <emphasis>stdout</emphasis> and <emphasis>stderr</emphasis> independently. With this option enabled logapp won't expect complete lines and will handle data as it comes in. By default all single data packets are written to a new line if this option is enabled, this can be changed with the <option>--alignlog</option> option. If the result will be usable depends on what kind of data is generated by the application.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--extended-regexp=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>If this option is enabled logapp will interpret provided regular expression patterns as extended regular expressions. The default is to use basic regular expressions.</para>
          </listitem>
        </varlistentry>

      </variablelist>

    </refsect2>

    <refsect2 id="logging_options">
      <title>LOGGING OPTIONS</title>
      <para>This section contains options that affect the logfile.</para>
      <variablelist>

        <varlistentry>
          <term><option>-l</option></term>
          <term><option>--logfile=</option><replaceable>file</replaceable></term>
          <listitem>
            <para>This option can be used to change the file that is used for storing the logged application data. If an empty string is provided, logging is disabled and no logfile will be created. The default is that logapp creates a logfile called <emphasis>logapp.log</emphasis> in the current directory.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-a</option></term>
          <term><option>--appendlog=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option specifies if the logfile will be truncated or if the data will be appended to an existing file on logapp startup.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--maxlogsize=</option><replaceable>kibyte</replaceable></term>
          <listitem>
            <para>To limit the maximum size of the logfile you can set this option to a value between <emphasis>10 and 4000000 kiBytes</emphasis>. The default is <emphasis>0</emphasis> which disables the logfile size limit. There are different ways implemented how the logfile is limited. Have a look at the options <option>--logrename</option> and <option>--circularlog</option> to learn more. The default way is that the extension <literal>.old</literal> is added to the logfile and a new logfile is started.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--logrename=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option specifies the behavior when a logfile is to be truncated. If <option>--logrename</option> is enabled the logfile is renamed. The new filename will be the same as before with the extension defined with <option>--oldlogext</option> added. The default extension is <literal>.old</literal>. This option is used together with the value of <option>--appendlog</option> and <option>--maxlogsize</option></para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--circularlog=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>If this option is enabled together with a logfile size limit set with <option>--maxlogsize</option>, the logfile will be used in a circular way. This means if the maximum size is reached, the file pointer is set to the beginning of the file and the old content is overwritten from the beginning. There are tags added to the logfile to help navigating in the file.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--oldlogext=</option><replaceable>extension</replaceable></term>
          <listitem>
            <para>This defines the extion that is used when logapp is renaming a logfile. The <option>--logrename</option> option defines if logapp will rename the file and the default extension is <literal>.old</literal>.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--locklogfile=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>With this option active the logfile is locked in order to prevent it to be overwritten by another task. This is useful if otherwise an unreadable mix up of different contents would be the result. Depending on the value of the <option>--maxaltlogfiles</option> option another logfile is chosen with the same name and a number added. Logfile locking is activated by default.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--warnlogfilelock=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This options defines if there should be a warning printed to the console if the chosen logfile is already locked or in other means not accessible. In this case there will be a message before the application is started and directly after its execution where the name of the alternative logfile is mentioned. This option is enabled by default. Also have a look at the <option>--printlogname</option> where you can define to always get the current logfile reported.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--printlogname=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option defines if the name of the used logfile should be printed after the application has finished its execution. This option is disabled by default. Also have a look at the <option>--warnlogfilelock</option> where you can enable/disable a warning if the logfile name is changed because of a locked logfile.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--maxaltlogfiles=</option><replaceable>number</replaceable></term>
          <listitem>
            <para>This options defines the maximum number that can be added to the logfile name, if the original file is not accessible. On logapp startup it will be checked if the currently defined logfile is writeable, if this is not the case automatically a number is added to the filename. If the alternative file is also not accessible this number is increased until a file is writable or the value of <replaceable>maxaltlogfiles</replaceable> is reached. In the latter case the application will exit with an error. If a value of 0 is used only the original logfile name is tried. Also have a look at the <option>--warnlogfilelock</option> and <option>--printlogname</option> options to define if there should be messages about the currently used logfile.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--alignlog=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option is used together with <option>--stdout_charbased</option> and <option>--stderr_charbased</option> and defines if data packets are written to the logfile as they come or if they are each written to a new line. The default is that each data packet is written to a new line, set this option to false to disable it.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--alignlinebreaks=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option is used together with <option>--stdout_charbased</option> and <option>--stderr_charbased</option> and aligns the lines to the left in the logfile with regard to prefix and timestamp. This option is enabled by default.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--jointimeout=</option><replaceable>time</replaceable></term>
          <listitem>
            <para>This option is used together with <option>--stdout_charbased</option> and <option>--stderr_charbased</option> and defines a ms timeout for joining single packets to one. This means if for example two chars get written within the timeout, they are treated as one packet. This is best used together with <option>--alignlog</option> and <option>--logtime</option>. Use this option if the data packets have lost their coherency for some reason (e.g. if the data comes through a serial line). This feature is disabled by default and can be enabled by setting <replaceable>time</replaceable> to a value bigger than 0 ms.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-t</option></term>
          <term><option>--logtime=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option can be enabled to add a ms timestamp to each line of the logfile. Normally the time since the application start is used, but this can be changed with the <option>--logreltime</option> option.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--logreltime=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>If this option is set this to true, the <option>--logreltime</option> option will use the relative time since the last line for the logged timestamps.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--logenv=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>With this option set to true logapp will add a list of all active environment variables to the logfile. This option is disabled by default.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-p</option></term>
          <term><option>--stdout_lineprefix=</option><replaceable>prefix</replaceable></term>
          <listitem><anchor id="stdout_lineprefix"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-P</option></term>
          <term><option>--stderr_lineprefix=</option><replaceable>prefix</replaceable></term>
          <listitem>
            <para>To be able to distinguish <emphasis>stdout</emphasis> and <emphasis>stderr</emphasis> output in the logfile logapp can prefix each line with a string that indicates if the line belongs to a specific data stream. Those strings can be changed with this option. The default is that <emphasis>stdout</emphasis> does not have a prefix and <emphasis>stderr</emphasis> is prefixed with <emphasis>STDERR:</emphasis>.</para>
          </listitem>
        </varlistentry>

      </variablelist>
    </refsect2>

    <refsect2 id="console_output_options">
      <title>CONSOLE OUTPUT OPTIONS</title>
      <para>This section contains options that affect the visual output on the console.</para>
      <variablelist>

        <varlistentry>
          <term><option>--dumbterm=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This option disables output coloring. This is usually done automatically if a <emphasis>dumb</emphasis> terminal is detected.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-s</option></term>
          <term><option>--print_summary=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>If this option is set to true, then a short summary will be printed after the application has terminated. This option is disabled by default.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-f</option></term>
          <term><option>--stdout_fgcol=</option><replaceable>color</replaceable></term>
          <listitem><anchor id="stdout_fgcol"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-F</option></term>
          <term><option>--stderr_fgcol=</option><replaceable>color</replaceable></term>
          <listitem>
            <para>This options define the foreground color for the  specific data stream. The value can be one of the entries in the <emphasis>console color table</emphasis> at the end of this section.</para>
            
          </listitem>
        </varlistentry>


        <varlistentry>
          <term><option>-b</option></term>
          <term><option>--stdout_bold=</option><replaceable>bool</replaceable></term>
          <listitem><anchor id="stdout_bold"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-B</option></term>
          <term><option>--stderr_bold=</option><replaceable>bool</replaceable></term>
          <listitem>
            <para>This options define if the font for the specific data stream should be printed bold.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-r</option></term>
          <term><option>--stdout_regexp=</option><replaceable>regular expression</replaceable></term>
          <listitem><anchor id="stdout_regexp"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-R</option></term>
          <term><option>--stderr_regexp=</option><replaceable>regular expression</replaceable></term>
          <listitem>
            <para>The regular expression that can be defined with this option is applied to every line of the specific data stream. On a match the background color changes to the value provided with the <option>--stdout_regexp_bgcol</option> respectively <option>--stderr_regexp_bgcol</option> option.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--stdout_regexp_bgcol=</option><replaceable>color</replaceable></term>
          <listitem><anchor id="stdout_regexp_bgcol"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--stderr_regexp_bgcol=</option><replaceable>color</replaceable></term>
          <listitem>
            <para>This options define the background color for the specific data stream for the case that the appropriate regular expression provided with <option>--stdout_regexp</option> or <option>--stderr_regexp</option> matches. The value can be one of the entries in the <emphasis>console color table</emphasis> at the end of this section.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-c</option></term>
          <term><option>--stdout_clip=</option><replaceable>width</replaceable></term>
          <listitem><anchor id="stdout_clip"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-C</option></term>
          <term><option>--stderr_clip=</option><replaceable>width</replaceable></term>
          <listitem>
            <para>This options define at which column the output should be clipped for the specific stream to reduce the amount of data written to the console. If a value of <emphasis>&minus;1</emphasis> is provided clipping is disabled for the stream. A value of <emphasis>&minus;2</emphasis> sets the clipping to the current console width. It is also possible to use <emphasis>disable</emphasis> and <emphasis>auto</emphasis> instead of the numeric values. The default is that <emphasis>stdout</emphasis> is limited to the console width and that clipping is deactivated for <emphasis>stderr</emphasis>.</para>
            <table id="color_table" frame="all" colsep="1" rowsep="1">
              <title>Console color table</title>
              <tgroup cols="2" align="left">
                <colspec colnum="1" colname="nr" colwidth="1*"/>
                <colspec colnum="2" colname="color" colwidth="1*"/>
                <thead>
                  <row>
                    <entry>#</entry>
                    <entry>color</entry>
                  </row>
                </thead>
                <tbody>
                  <row>
                    <entry>&minus;1</entry>
                    <entry>(console) default</entry>
                  </row>
                  <row>
                    <entry>0</entry>
                    <entry>black</entry>
                  </row>
                  <row>
                    <entry>1</entry>
                    <entry>red</entry>
                  </row>
                  <row>
                    <entry>2</entry>
                    <entry>green</entry>
                  </row>
                  <row>
                    <entry>3</entry>
                    <entry>brown</entry>
                  </row>
                  <row>
                    <entry>4</entry>
                    <entry>blue</entry>
                  </row>
                  <row>
                    <entry>5</entry>
                    <entry>magenta</entry>
                  </row>
                  <row>
                    <entry>6</entry>
                    <entry>cyan</entry>
                  </row>
                  <row>
                    <entry>7</entry>
                    <entry>white</entry>
                  </row>
                </tbody>
              </tgroup>
            </table>
            <para></para>
          </listitem>
        </varlistentry>

      </variablelist>
    </refsect2>

    <refsect2 id="command_execution_options">
      <title>COMMAND EXECUTION OPTIONS</title>
      <para>This section contains options that configure the execution of commands on regular expression matches.</para>
      <variablelist>

        <varlistentry>
          <term><option>--exitonexecfail=</option><replaceable>BOOL</replaceable></term>
          <listitem>
            <para>This option defines if logapp should exit and end the wrapped application if the return value of an executed command indicates a failure. As default this option is disabled and logapp ignores the return state of executed commands.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--preexec=</option><replaceable>command</replaceable></term>
          <listitem>
            <para>The command that can be provided with this option is executed directly before the application is started. At this time the header is already written to the logfile and can be parsed by the command.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>--postexec=</option><replaceable>command</replaceable></term>
          <listitem>
            <para>The command that can be provided with this option is executed directly after the application has exited. At this time the logfile is already closed for writing so all application output and the footer are already included and can be processed by the command.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-e</option></term>
          <term><option>--stdout_execregexp=</option><replaceable>regular expression</replaceable></term>
          <listitem><anchor id="stdout_execregexp"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-E</option></term>
          <term><option>--stderr_execregexp=</option><replaceable>regular expression</replaceable></term>
          <listitem>
            <para>The regular expression that can be defined with this option is applied to every line of the specific data stream. On a match the command provided with the <option>--stdout_execcommand</option> respectively <option>--stderr_execcomand</option> option is executed. An empty value for this option disables the regular expression matching.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><option>-x</option></term>
          <term><option>--stdout_execcommand=</option><replaceable>command</replaceable></term>
          <listitem><anchor id="stdout_execcommand"/></listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-X</option></term>
          <term><option>--stderr_execcommand=</option><replaceable>command</replaceable></term>
          <listitem>
            <para>This option defines the command that is executed on a regular expression match. The regular expression can be defined separately for the <emphasis>stdout</emphasis> and <emphasis>stderr</emphasis> stream with the <option>--stdout_execregexp</option> respectively <option>--stderr_execregexp</option> option.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsect2>
  </refsect1>
	
  <refsect1 id="regular_expressions">
    <title>REGULAR EXPRESSIONS</title>
    <para>Regular expressions are patterns that describe strings. Logapp uses this patterns to execute actions based on strings found in the data stream. The implementation is identical to the one that is used by <emphasis>grep</emphasis>.</para>
    <para>Logapp understands the "basic" and "extended" syntax of regular expressions as defined by POSIX. The default is to use the basic set, but you can switch to extended patterns with the <option>--extended-regexp</option> parameter. Please have a look at the <citerefentry><refentrytitle>grep</refentrytitle><manvolnum>1</manvolnum></citerefentry> and <citerefentry><refentrytitle>regex</refentrytitle><manvolnum>7</manvolnum></citerefentry> manpage for detailed information.</para>

    <refsect2 id="regular_expression_examples">
        <title>EXAMPLES</title>
        <variablelist>
            <varlistentry>
                <term><option>String</option></term>
                <listitem>
                    <para>Matches "String"</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>^String</option></term>
                <listitem>
                    <para>Matches "String" at the beginning of a line</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>String$</option></term>
                <listitem>
                    <para>Matches "String" at the end of a line</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>^String$</option></term>
                <listitem>
                    <para>Line contains only "String"</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>[Ss]tring</option></term>
                <listitem>
                    <para>Matches "String" or "string"</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>Str.ng</option></term>
                <listitem>
                    <para>The dot matches all characters, so this matches for example "String" or "Strong"</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>Str.*ng</option></term>
                <listitem>
                    <para>The dot together with star matches any number of characters, so this matches for example "String" or "Streaming"</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>^[A-Z] *</option></term>
                <listitem>
                    <para>Matches any one of the characters from A to Z at the beginning of a line followed by zero or any number of spaces</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>String\|Word</option></term>
                <listitem>
                    <para>Matches "String" or "Word" when working with <emphasis>basic regular expressions</emphasis></para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term><option>String|Word</option></term>
                <listitem>
                    <para>Matches "String" or "Word" when working with <emphasis>extended regular expressions</emphasis></para>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsect2>
  </refsect1>

  <refsect1 id='environment'>
    <title>ENVIRONMENT</title>
    <variablelist>
      <varlistentry>
        <term><envar>TERM</envar></term>
        <listitem>
          <para>This variable is checked to see which type of console logapp is running in. Currently only the value <emphasis>dumb</emphasis> is handled in a special way &mdash; by disabling console colors. If the <envar>TERM</envar> variable is missing also a dumb terminal is assumed. The setting can be overridden by enabling/disabling the dumb terminal mode using the <option>--dumbterm</option> option.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1 id='files'>
    <title>FILES</title>
    <variablelist>
      <varlistentry>
        <term><filename>~/.logapprc</filename></term>
        <term><filename>/etc/logapp.conf</filename></term>
        <term><filename>/etc/logapp/logapp.conf</filename></term>
        <listitem>
          <para>Configuration file locations that are tried if no <option>--configfile</option> option is provided.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1 id='bugs'>
    <title>BUGS</title>
    <para>See the <filename>TODO</filename> file included in the source package.</para>
  </refsect1>
  <refsect1 id='see_also'>
    <title>SEE ALSO</title>
    <para><citerefentry><refentrytitle>grep</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>regex</refentrytitle><manvolnum>7</manvolnum></citerefentry></para>
  </refsect1>
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>
      <author>
        <firstname>Michael</firstname>
        <surname>Brunner</surname>
        <contrib>Original author</contrib>
      </author>
    </para>
  </refsect1>

  <refsect1 id='copying'>
    <title>COPYING</title>
    <para>Copyright (C) 2007&ndash;2011 Michael Brunner</para>
    <para>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.</para>
    <para>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the General Public License for more details.</para>
  </refsect1>

</refentry>
