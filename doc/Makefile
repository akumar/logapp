# Makefile for Logapp documentation
#
# Copyright (C) 2007-2009 Michael Brunner <mibru@gmx.de>

HTMLXSL = /usr/share/sgml/docbook/stylesheet/xsl/nwalsh/html
MANPAGE = logapp.1
HTMLPAGE = manpage.html
TEXTPAGE = manpage.txt
SOURCEFILE = manpage.xml
MAINMANPAGE = ../$(MANPAGE)
EXTRADEPS = Makefile

all: html text manpage

html: $(HTMLPAGE)
$(HTMLPAGE): $(SOURCEFILE) $(EXTRADEPS)
	xsltproc $(HTMLXSL)/docbook.xsl $(SOURCEFILE) \
		| sed -e "s/&#8722;/-/g" > $(HTMLPAGE)

manpage: $(MANPAGE)
$(MANPAGE): $(SOURCEFILE) $(EXTRADEPS)
	docbook2x-man --to-stdout --encoding "us-ascii" manpage.xml |\
	sed "1,1s/'/./" > $(MANPAGE)

text: $(TEXTPAGE)
$(TEXTPAGE): $(HTMLPAGE) $(EXTRADEPS)
	lynx -dump $(HTMLPAGE) > $(TEXTPAGE)

update-main: manpage $(MAINMANPAGE)
$(MAINMANPAGE): $(MANPAGE)
	install -m 644 $(MANPAGE) $(MAINMANPAGE)

clean:
	rm -f $(MANPAGE) *.log tags *.html *.gz *.txt

distclean: clean

.PHONY: all clean distclean html manpage text update-main
