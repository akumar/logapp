# Makefile for Logapp
#
# Copyright (C) 2007-2017 Michael Brunner <mibru@gmx.de>

TARGET = logapp
MANPAGE = logapp.1
VERSION = 0.16

### Configuration options: ###

# Enable support for pseudo terminal interface on stdout
# -> enables --usepty runtime option
SUPPORT_PTY = 1

# Use different threads for stdout and stderr handling
USE_THREADS = 1

# Build static binary
BUILD_STATIC ?= 0

# Add architecture prefix here or use CROSS_COMPILE environment variable to
# specify a cross compiler
CROSS_COMPILE ?=

# Set this to 1 to create a debug build of logapp (can also be set as
# environment variable)
DEBUG_BUILD ?= 0

# This defines which symlinks are created during the installation
SYMLINKS = logmake logsvn logcvs
PREFIX = $(DESTDIR)/usr/
BINDESTDIR = $(PREFIX)bin/
MANDIR = $(PREFIX)share/man/man1/

# Tool definitions
INSTALL = install -c

### END OF CONFIGURATION SECTION ###

# If diet is defined with the CROSS_COMPILE variable be sure to add a space
# so the code is linked against the dietlibc
ifeq ($(CROSS_COMPILE), diet)
	CROSS_COMPILE = diet 
	LINK_DIETLIBC = 1
endif

CC = $(CROSS_COMPILE)gcc
DEFS    = -DSVN_REVISION='"$(shell svnversion -cn . 2>/dev/null \
	| sed -e "s/.*://" -e "s/\([0-9]*\).*/\1/")"' \
	 -DVERSION='"$(VERSION)"' \
	 -DDEBUG_BUILD="$(DEBUG_BUILD)" \
	 -DEXECUTABLE='"$(TARGET)"' \
	 -DCONFIG_SUPPORT_PTY="$(SUPPORT_PTY)" \
	 -DCONFIG_USE_THREADS="$(USE_THREADS)"
 
# Do not optimize code when creating a debug build
ifeq ($(DEBUG_BUILD),1)
	CFLAGS = -g -O0
	LDFLAGS =
endif

# Use DPKG default buildflags, on Debian based systems
DPKG_CFLAGS = $(shell dpkg-buildflags --get CFLAGS 2>/dev/null)
ifeq "$(DPKG_CFLAGS)" ""
	CFLAGS ?= -O2
else
	CFLAGS ?= $(DPKG_CFLAGS)
endif

CFLAGS += -Wall -Wextra $(DEFS)

LINK = $(CROSS_COMPILE)gcc

ifeq ($(BUILD_STATIC),1)
	LDFLAGS += --static
endif
  
DPKG_LDFLAGS = $(shell dpkg-buildflags --get LDFLAGS 2>/dev/null)
ifeq "$(DPKG_LDFLAGS)" ""
	LDFLAGS ?= 
else
	LDFLAGS ?= $(DPKG_LDFLAGS)
endif

# Strip unless we are creating a debug build
ifneq ($(DEBUG_BUILD),1)
	LDFLAGS += -s
endif

OBJECTS = main.o configuration.o logfile.o capture.o

DEPENDENCIES = .dependencies
EXTRADEPS = Makefile

LIBS = 
ifeq ($(SUPPORT_PTY),1)
ifneq ($(LINK_DIETLIBC),1)
	LIBS += -lutil
endif
endif
ifeq ($(USE_THREADS),1)
	LIBS += -lpthread
endif

all: $(DEPENDENCIES) $(TARGET)

$(TARGET): $(OBJECTS)
	$(LINK) $(LDFLAGS) -o $(TARGET) $(OBJECTS) $(LIBS)

install: all install_links
	install -d $(BINDESTDIR)
	install -t $(BINDESTDIR) $(TARGET)
	install -d $(MANDIR)
	install -m 644 -t $(MANDIR) $(MANPAGE)

install_links:
	for L in $(SYMLINKS); do ln -s -f $(TARGET) $(BINDESTDIR)$$L ; done

uninstall: deinstall

deinstall: remove_links
	rm -f $(BINDESTDIR)$(TARGET)
	rm -f $(MANDIR)$(MANPAGE)

remove_links:
	for L in $(SYMLINKS); do rm -f $(BINDESTDIR)$$L ; done

clean:
	rm -f *.o $(TARGET) $(DEPENDENCIES)
	@make -C doc clean

distclean: clean
	rm -f *.log tags

man:
	@$(MAKE) -C doc update-main

help:
	@echo 'Generic targets:'
	@echo ' install         - Install logapp to $(DESTDIR)'
	@echo ' uninstall       - Uninstall logapp from $(DESTDIR)'
	@echo ' clean           - Remove generated files from build directory'
	@echo ''
	@echo 'Build targets:'
	@echo ' all             - Build logapp' 
	@echo ''
	@echo 'Environment variables:'
	@echo ' PREFIX          - Install prefix (default: /usr/local)'
	@echo ' DEBUG_BUILD     - Create debug build (default: 0)'
	@echo ' BUILD_STATIC    - Create static build (default: 0)'
	@echo ' CROSS_COMPILE   - Use cross compile prefix (default: )'

$(DEPENDENCIES):
	@$(CC) -MM *.c | sed -e 's/\([^\]\)$$/\1 $(EXTRADEPS)/' \
		> $(DEPENDENCIES)

.PHONY: all clean distclean deinstall uninstall install install_links \
	remove_links man

-include $(DEPENDENCIES)
